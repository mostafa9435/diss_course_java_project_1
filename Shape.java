package de.tuhh.diss.plotbot.shape;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.util.Delay;

public class Shape {
	
	private int length, width;
	private boolean rectangle, ship;
	
	
	public Shape(boolean rectangle, boolean ship){
		this.rectangle = rectangle;
		this.ship = ship;
		setUserInput(rectangle, ship);
	}

	public void setUserInput(boolean rectangle, boolean ship){
	    if (rectangle == true && ship == false){
	    	LCD.clearDisplay();
	    	LCD.drawString("Enter the dimension(s)", 0, 0);
	    	LCD.drawString("Length: ",0 , 1);
	    	this.length = setDrawnInteger(2);
	    	LCD.drawString("Width: ",0 , 4);
	    	this.width = setDrawnInteger(5);
	    	}
	    if (rectangle == false && ship == true){
	    	LCD.clearDisplay();
	    	LCD.drawString("Enter the dimension(s)", 0, 0);
	    	LCD.drawString("Width: ",0 , 1);
	    	this.width = setDrawnInteger(2);
	    	}
	    }
	
	public int setDrawnInteger(int line){
		int n[] = {0, 0, 0};
		int digitCount = 0;
		boolean complete = false;
		while(digitCount < 3 && complete == false){
			
			LCD.drawInt(n[0], 0, line);
			LCD.drawInt(n[1], 1, line);
			LCD.drawInt(n[2], 2, line);
			
			if(Button.RIGHT.isDown()){
				n[digitCount]++;
				if (n[digitCount] > 9){
					n[digitCount] = 9;
					}
				//LCD.drawInt(n[digitCount], digitCount, 2);
				Delay.msDelay(500);
				}
			if(Button.LEFT.isDown()){
				n[digitCount]--;
				if (n[digitCount] < 0){
					n[digitCount] = 0;
					}
				//LCD.drawInt(n[digitCount], digitCount, 2);
				Delay.msDelay(500);
				}
			if(Button.ENTER.isDown()){
				digitCount++;
				if( digitCount > 2){
					complete = true;
					}
				Delay.msDelay(500);
				}
			}
		
		return concatInteger(n);
		}
	
	public int concatInteger(int n []){
		String concat = Integer.toString(n[0]) + Integer.toString(n[1]) + Integer.toString(n[2]);
		int combined = Integer.parseInt(concat);
		return combined;
	}
	
	public int getLength(){
		return length;
	}
	
	public int getWidth(){
		return width;
	}
	
	
}