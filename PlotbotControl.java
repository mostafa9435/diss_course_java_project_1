package de.tuhh.diss.plotbot;

import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.util.Delay;

/**
 * This Class is used for control of the plotting robot. A great amount of time
 * should spend for controlling the robot. Add a suitable constructor and add
 * further methods you need for driving the motors, evaluating the sensors etc.
 */
public class PlotbotControl {

	private TouchSensor armSensor;
	private TouchSensor penSensor;
	private LightSensor lightSensor;
	public final int MAX_ANGLE_COUNT = -4774;
	public final int MAX_PEN_HEIGHT = 289;
	public final int WHEEL_DIAMETER = 56;
	public final int WHEEL_RADIUS = WHEEL_DIAMETER / 2;
	public final int LIGHT_SENSOR_RADIUS = 105;
	public final int PEN_RADIUS = 80;
	public final int CLEARANCE = LIGHT_SENSOR_RADIUS - PEN_RADIUS;
	public final int WHEEL_GEAR_RATIO = 5;
	public final int ARM_GEAR_RATIO = 84;
	public final int Y_MAX = 230;
	public final int MAX_ARM_ANGLE = 45;
	public final double DEGREES_TO_RADIANS = Math.PI / 180;
	public final double RADIANS_TO_DEGREES = 180 / Math.PI;

	private double xCurrent;
	private double yCurrent;

	public PlotbotControl() {
		armSensor = new TouchSensor(SensorPort.S1);
		penSensor = new TouchSensor(SensorPort.S2);
		lightSensor = new LightSensor(SensorPort.S3);
	}

	public TouchSensor getArmSensor() {
		return armSensor;
	}

	public TouchSensor getPenSensor() {
		return penSensor;
	}

	public LightSensor getLightSensor() {
		return lightSensor;
	}

	public int getMappedWheelMotorAngle(double distance) {
		int wheel_radius = WHEEL_DIAMETER / 2;
		double theta_wheel = distance / (DEGREES_TO_RADIANS * wheel_radius);
		int theta_motor = (int) Math.round(theta_wheel * WHEEL_GEAR_RATIO);
		return theta_motor;
	}

	public int getMappedArmMotorAngle(int width) {
		double opp, hyp, sin_angle, theta_rad, theta_arm;
		int theta_motor;
		opp = (double) width;
		hyp = (double) PEN_RADIUS;
		sin_angle = opp / hyp;
		theta_rad = Math.asin(sin_angle);
		theta_arm = Math.toDegrees(theta_rad);
		theta_motor = -1 * (int) (Math.round(theta_arm * ARM_GEAR_RATIO)); // This
																			// is
																			// also
																			// the
																			// tacho
																			// count!!
		return theta_motor;
	}

	public void angleToXy(double angleA, double angleC) { // angle A = swivel
															// motor
		// angle, angle C = wheel
		// motor angle

		this.xCurrent += this.PEN_RADIUS * Math.sin(angleA);
		this.yCurrent = (((angleC * this.DEGREES_TO_RADIANS) * this.WHEEL_RADIUS) / (this.WHEEL_GEAR_RATIO))
				- (this.PEN_RADIUS - (this.PEN_RADIUS * Math.cos(angleA)));



	}

	public double[] xyToAngle(double x, double y) { // input parameters are
													// delta jumps from initial
													// coordinate to new
													// coordinate
													// the returned angles are
													// the angles to be inputed
													// to the motors to achieve
													// such a jump
		double angleA = 0.0;
		double angleC = 0.0;
		double ySwivel;
		double yVertical;

		if (x == 0) { // hence only vertical motion parallel to the y -
						// direction is required (i.e. swivel arm fixed)
			angleC = (y * (this.WHEEL_GEAR_RATIO) * (this.RADIANS_TO_DEGREES)) / ((this.WHEEL_DIAMETER) / 2);

		} else {
			if ((x > 0 && Motor.A.getTachoCount() > 0) || (x < 0 && Motor.A.getTachoCount() < 0)) {
				angleA = 90 - Math.abs(Motor.A.getTachoCount())
						- Math.acos((Math.abs(x) + this.PEN_RADIUS
								* Math.sin(Math.abs(Motor.A.getTachoCount()) * this.DEGREES_TO_RADIANS))
								/ this.PEN_RADIUS);

				ySwivel = this.PEN_RADIUS * Math.cos(Math.abs(Motor.A.getTachoCount()) * this.DEGREES_TO_RADIANS)
						- this.PEN_RADIUS * Math.sin(Math.acos((Math.abs(x) + this.PEN_RADIUS
								* Math.sin(Math.abs(Motor.A.getTachoCount()) * this.DEGREES_TO_RADIANS))
								/ this.PEN_RADIUS));

				yVertical = y + ySwivel;
				
			}
			else {
				angleA = Math.abs(Motor.A.getTachoCount()) - Math
						.asin((this.PEN_RADIUS * Math.sin(Math.abs(Motor.A.getTachoCount())) - x) / this.PEN_RADIUS);
				ySwivel = (this.PEN_RADIUS * Math
						.asin((this.PEN_RADIUS * Math.sin(Math.abs(Motor.A.getTachoCount())) - x) / this.PEN_RADIUS))
						- this.PEN_RADIUS * Math.sin(Math.abs(Motor.A.getTachoCount()));
				yVertical=y-ySwivel;

			}
			angleC = (yVertical * (this.WHEEL_GEAR_RATIO) * (this.RADIANS_TO_DEGREES))
					/ ((this.WHEEL_DIAMETER) / 2);

		}
		if (x > 0) {
			angleA = -1 * angleA;
		}
		if (y < 0) {
			angleC = -1 * angleC;
		}

		double[] doubleArr = { angleA, angleC };
		return doubleArr;
	}

	public double getxCurrent() {
		return xCurrent;
	}

	public double getyCurrent() {
		return yCurrent;
	}
}
