package de.tuhh.diss.plotbot.shape;

import de.tuhh.diss.plotbot.PlotbotControl;

public class Coord {
	private PlotbotControl pc;
	private int x;
	private int y;
	
	public Coord(PlotbotControl pc, int x, int y){
		this.pc = pc;
		this.x = x;
		this.y = y;
	}

	

	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
