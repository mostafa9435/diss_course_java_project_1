package de.tuhh.diss.plotbot.shape;

import de.tuhh.diss.plotbot.shape.Plottable;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.util.Delay;

import javax.microedition.location.Coordinates;

import de.tuhh.diss.plotbot.PlotbotControl;

public class Rectangle implements Plottable {
	private int length, width;
	private final int MAX_LENGTH = 230;
	private final int MIN_LENGTH = 1;
	private final int MIN_WIDTH = 1;
	private final double DEGREES_TO_RADIANS = Math.PI / 180;
	private Coord pen_position;
	private PlotbotControl pc;

	public Rectangle(PlotbotControl pc) {
		Shape Rectangle = new Shape(true, false);
		this.length = Rectangle.getLength();
		this.width = Rectangle.getWidth();
		checkOverBelowSize();
		this.pc = pc;
		plot(this.pc);
	}
	
	private void checkOverBelowSize() {
		double theta = pc.MAX_ARM_ANGLE;
		int max_width =  (int) Math.cos(theta);
		if (width > max_width || length > MAX_LENGTH) {
			LCD.clearDisplay();
			LCD.drawString("Values are too big!", 0, 0);
			LCD.drawString("Enter a length less", 0, 2);
			LCD.drawString("than: ", 0, 3);
			LCD.drawInt(MAX_LENGTH, 0, 4);
			LCD.drawString("and a width less", 0, 6);
			LCD.drawString("than: ", 0, 7);
			LCD.drawInt(max_width, 0, 8);
			Delay.msDelay(5000);
			new Rectangle(pc);
		}
		if (width < MIN_WIDTH || length < MIN_LENGTH){
			LCD.clearDisplay();
			LCD.drawString("Values are too small!", 0, 0);
			LCD.drawString("Enter a length more", 0, 2);
			LCD.drawString("than: ", 0, 3);
			LCD.drawInt(MIN_LENGTH, 0, 4);
			LCD.drawString("and a width more", 0, 6);
			LCD.drawString("than: ", 0, 7);
			LCD.drawInt(MIN_WIDTH, 0, 8);
			Delay.msDelay(5000);
			new Rectangle(pc);	
		}
		
	}

	public void plot(PlotbotControl pc) {
		// put your plot routine in here

		Calibration c = new Calibration(pc);
		Delay.msDelay(1000);
		moveToInitialRectanglePosition();
		Delay.msDelay(1000);
		plotAlignedVerticalSide();
		Delay.msDelay(1000);
		plotLowerHorizontalSide();
		Delay.msDelay(1000);
		plotDisplacedVerticalSide();
		Delay.msDelay(1000);
		plotUpperHorizontalSide();

	}

	public void moveToInitialRectanglePosition() {
		/*
		 * int clearance = pc.LIGHT_SENSOR_RADIUS - pc.PEN_RADIUS; double
		 * distance = (double) (pc.Y_MAX + clearance); int theta_motor =
		 * pc.getMappedWheelMotorAngle(distance); Motor.C.setSpeed(400);
		 * Motor.C.rotate(theta_motor); pen_position = new Coord(this.pc, 0,
		 * pc.Y_MAX);
		 */


		double angle = ( pc.Y_MAX * pc.WHEEL_GEAR_RATIO ) /   (pc.WHEEL_RADIUS);
		angle *= pc.RADIANS_TO_DEGREES;
		Motor.C.rotate((int)Math.round(angle));
		Delay.msDelay(15000);
		pc.angleToXy(Motor.A.getTachoCount(), Motor.C.getTachoCount());
		
		/*Motor.B.setSpeed(400);
		LCD.clearDisplay();
		Motor.B.backward(); // descend pen
		Delay.msDelay(200);
		while (Motor.B.getTachoCount() < pc.MAX_PEN_HEIGHT && !pc.getPenSensor().isPressed() ) {
			LCD.drawInt(Motor.B.getTachoCount(), 0, 0);
		}
		Motor.B.stop();*/
		
		Motor.B.setSpeed(400);
		LCD.clearDisplay();
		Motor.B.rotate(-1 * (pc.MAX_PEN_HEIGHT) ); // descend pen
		
		Delay.msDelay(10000);
		

	}

	public void plotAlignedVerticalSide() {
		/*
		 * Coord start = pen_position; int y_i = start.getY(); int y_f = y_i -
		 * length; Coord end = new Coord(this.pc,0,y_f); Line side = new
		 * Line(pc, start,end ); side.drawVerticalLine(); pen_position = end;
		 */

		Coord start = new Coord(this.pc, (int) pc.getxCurrent(), (int) pc.getyCurrent());

		int yFinal = (int) pc.getyCurrent() - this.length;
		Coord end = new Coord(this.pc, (int) pc.getxCurrent(), yFinal);

		Line l1 = new Line(this.pc, start, end);
		l1.drawVerticalLine();

	}

	public void plotLowerHorizontalSide() {
		// insert code here

		// JUST FOR TESTING//
		/*
		 * int y_i = pen_position.getY(); pen_position = new Coord(this.pc,
		 * width, y_i);
		 */

		Coord start = new Coord(this.pc, (int) pc.getxCurrent(), (int) pc.getyCurrent());

		int xFinal = (int) pc.getxCurrent() + this.width;
		Coord end = new Coord(this.pc, xFinal, (int) pc.getyCurrent());

		Line l2 = new Line(this.pc, start, end);
		l2.drawHorizontalLine();

	}

	public void plotDisplacedVerticalSide() {
		
		Coord start = new Coord(this.pc, (int) pc.getxCurrent(), (int) pc.getyCurrent());

		int yFinal = (int) pc.getyCurrent() + this.length;
		Coord end = new Coord(this.pc, (int) pc.getxCurrent(), yFinal);

		Line l3 = new Line(this.pc, start, end);
		l3.drawVerticalLine();
		
		
		
		
		
		
		
		/*// Pythagorean Theorem
		double opp, adj, hyp, y_i, y_component, y_displaced;
		opp = (double) width;
		hyp = (double) pc.PEN_RADIUS;
		adj = Math.sqrt(Math.pow(hyp, 2) - Math.pow(opp, 2));

		// Get the required values to move to the start coordinate
		y_component = adj;
		y_i = (double) pen_position.getY();
		y_displaced = y_i - y_component;
		int theta_wheel_motor = -1 * pc.getMappedWheelMotorAngle(y_displaced); // theta
																				// is
																				// negative
																				// to
																				// move
																				// backward

		// Move Vehicle backward to the appropriate start position
		Motor.C.setSpeed(400);
		Motor.C.rotate(theta_wheel_motor);

		// ....Assuming the horizontal line is not drawn....//

		// Rotate arm clockwise to the desired width
		int theta_arm_motor = pc.getMappedArmMotorAngle(width);
		Motor.A.setSpeed(400);
		Motor.A.rotate(theta_arm_motor);

		// Draw line
		Coord start = pen_position;
		int x_f = start.getX();
		int y_f = start.getY() + length;
		Coord end = new Coord(this.pc, x_f, y_f);
		Line side = new Line(pc, start, end);
		side.drawVerticalLine();
		pen_position = end;*/

	}

	public void plotUpperHorizontalSide() {
		
		Coord start = new Coord(this.pc, (int) pc.getxCurrent(), (int) pc.getyCurrent());

		int xFinal = (int) pc.getxCurrent() - this.width;
		Coord end = new Coord(this.pc, xFinal, (int) pc.getyCurrent());

		Line l4 = new Line(this.pc, start, end);
		l4.drawHorizontalLine();
		/*// insert code here

		// JUST FOR TESTING//

		int y_i = pen_position.getY();
		pen_position = new Coord(this.pc, 0, y_i);
*/
	}

}



