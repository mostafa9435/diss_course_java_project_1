package de.tuhh.diss.plotbot.shape;

import de.tuhh.diss.plotbot.shape.Plottable;
import lejos.nxt.LCD;
import lejos.util.Delay;
import de.tuhh.diss.plotbot.PlotbotControl;

public class Ship implements Plottable{
	private int width;
	private int bottom;
	private int height;
	private int big_sail_base;
	private int small_sail_base;
	private int big_sail_height;
	private int small_sail_height; 
	private int midpoint;
	 
	private int X_0 = 0;
	private int Y_0 = 0;
	
	private final int MINIMUM_WIDTH = 4;
	private final int FLAG_HEIGHT = 1;
	private final int EDGE_CLEARANCE = 1;
	private final int SAIL_CLEARANCE = 1;
	
	
	private PlotbotControl pc;
	
	public Ship(PlotbotControl pc){
		Shape Ship = new Shape( false, true);
		this.width = Ship.getWidth();
		
		checkOverBelowSize();
		getDimensionForOddOrEven();
		
		this.pc = pc;
		plot(pc);
	}
	

	private void checkOverBelowSize() {
		double theta = pc.MAX_ARM_ANGLE;
		int max_width =  (int) Math.cos(theta);
		if (width > max_width){
			LCD.clearDisplay();
			LCD.drawString("Value is too big!", 0, 0);
			LCD.drawString("Enter a value less", 0, 2);
			LCD.drawString("than: ", 0, 3);
			LCD.drawInt(max_width, 0, 4);
			Delay.msDelay(5000);
			new Ship(pc);
		}
		if (width < MINIMUM_WIDTH){
			LCD.clearDisplay();
			LCD.drawString("Value is too small!", 0, 0);
			LCD.drawString("Enter a value more", 0, 2);
			LCD.drawString("than: ", 0, 3);
			LCD.drawInt(MINIMUM_WIDTH, 0, 4);
			Delay.msDelay(5000);
			new Ship(pc);
		}
		
	}

	private void getDimensionForOddOrEven() {
		// TODO Check odd or even
		midpoint = width / 2; //if odd: rounded down automatically
		bottom = width - (2 * EDGE_CLEARANCE);
		
		if (width % 2 == 0){
			//even
			big_sail_base = bottom / 2;
			small_sail_base = big_sail_base;
			small_sail_height = small_sail_base;
			big_sail_height = small_sail_height + SAIL_CLEARANCE;
			
			}
		else{
			//odd
			big_sail_base = width - midpoint - EDGE_CLEARANCE;
			small_sail_base = midpoint - EDGE_CLEARANCE;
			small_sail_height = small_sail_base;
			big_sail_height = big_sail_base;	
			}
		
		height = EDGE_CLEARANCE + big_sail_height + FLAG_HEIGHT;
		
	}
	

	public void plot(PlotbotControl pc){
		// put your plot routine in here
		Calibration c = new Calibration(pc);
		Delay.msDelay(1000);
		moveToInitialShipPosition();
		Delay.msDelay(1000);
		plotWidth();
		Delay.msDelay(1000);
		plotRightInclined();
		Delay.msDelay(1000);
		plotBottom();
		Delay.msDelay(1000);
		plotLeftInclined();
		Delay.msDelay(1000);
		plotSmallSail();
		Delay.msDelay(1000);
		plotFlag();
		Delay.msDelay(1000);
		plotBigSail();
		
		
	}
	private void moveToInitialShipPosition() {
		// TODO Move to X_0 and Y_0
		
	}
	
	private void plotWidth() {
		// TODO DRAW HORIZONTAL LINE - left to right
		int x_initial = X_0;
		int y_initial = Y_0 + EDGE_CLEARANCE;
		Coord start = new Coord(pc, x_initial, y_initial);                                                           //                          
		                                                                                                             //          x----------------------x                                
		int x_final = X_0 + width;                                                                                   //
		int y_final = Y_0 + EDGE_CLEARANCE;
		Coord end = new Coord(pc, x_final, y_final);
		
	}
	
	private void plotRightInclined() {
		// TODO DRAW THE RIGHT INCLINED LINE - top to bottom
		int x_initial = X_0 + width;
		int y_initial = Y_0 + EDGE_CLEARANCE;
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //
		                                                                                                              //          x----------------------x 
		int x_final = X_0 + (width - EDGE_CLEARANCE);                                                                 //                                /
		int y_final = Y_0;                                                                                            //                               x
		Coord end = new Coord(pc, x_final, y_final);
		
	}
	
	private void plotBottom() {
		// TODO DRAW HORIZONTAL LINE - right to left
		int x_initial = X_0 + (width - EDGE_CLEARANCE);
		int y_initial = Y_0;
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //
		                                                                                                              //          x----------------------x 
		int x_final = X_0 + EDGE_CLEARANCE;                                                                           //                                /
		int y_final = Y_0;                                                                                            //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}

	private void plotLeftInclined() {
		// TODO DRAW THE LEFT INCLINE LINE - bottom to top
		int x_initial = X_0 + EDGE_CLEARANCE; 
		int y_initial = Y_0;
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //
		                                                                                                              //          x----------------------x 
		int x_final = X_0;                                                                                            //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE;                                                                           //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}
	
	private void getToTheSailStartCoordinate() {
		// TODO 
		int x_initial = X_0;                                                                                          //                  
		int y_initial = Y_0 + EDGE_CLEARANCE;                                                                         //                 
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                 
		                                                                                                              //          x----x-----------------x 
		int x_final = X_0 + EDGE_CLEARANCE;                                                                           //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE;;                                                                          //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}

	private void plotSmallSail() {
		// TODO DRAW SMALL SAIL HYPOTNEUSE - left bottom to right top                                                 
		// STEP 1: DRAW A SMALL HORIZONTAL           
		getToTheSailStartCoordinate();
		// STEP 2: DRAW HYPOTNEUS                                                                                     //
		int x_initial = X_0 + EDGE_CLEARANCE;                                                                         //                  x
		int y_initial = Y_0 + EDGE_CLEARANCE;                                                                         //                 /
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / 
		                                                                                                              //          x----x-----------------x 
		int x_final = X_0 + midpoint;                                                                                 //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE + small_sail_height;                                                       //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
		
	}
	
	private void plotFlag() {
		// TODO Auto-generated method stub
		//STEP 1: DRAW FLAG POLE - bottom to top
		drawPole();
		//STEP 2: DRAW UPPER HORIZONTAL - left to right
		drawUpperHorizontal();
		//STEP 3: DRAW RIGHT VERTICAL - top to bottom
		drawRightVertical();
		//STEP 4: DRAW LOWER HORIZONTAL - right to left
		drawLowerHorizontal();
	}
	
	private void drawPole() {
		// TODO DRAW VERTICAL LINES
		//STEP 1: DRAW THE LOWER PART OF THE POLE
		drawLowerPole();
		//STEP 2: DRAW THE UPPER PART OF THE POLE
		drawUpperPole();
		
	}

	private void drawLowerPole() {
		// TODO DRAW VERTICAL LINE - top to bottom
		                                                                                                              //
		                                                                                                              //
		                                                                                                              //
		int x_initial = X_0 + midpoint;                                                                               //                  x
		int y_initial = Y_0 + EDGE_CLEARANCE + small_sail_height;                                                     //                 /|
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |
		                                                                                                              //          x----x--x--------------x 
		int x_final = X_0 + midpoint;                                                                                 //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE;                                                                           //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}

	private void drawUpperPole() {
		// TODO DRAW VERTICAL LINE - bottom to up
        																											  //                  |
																													  //                  |
                                                                                                                      //                  |
		int x_initial = X_0 + midpoint;                                                                               //                  x
		int y_initial = Y_0 + EDGE_CLEARANCE;                                                                         //                 /|
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |
                                                                                                                      //          x----x--x--------------x 
		int x_final = X_0 + midpoint;                                                                                 //           \                    /
		int y_final = Y_0 + height;                                                                                   //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}

	private void drawUpperHorizontal() {
		// TODO DRAW HORIZONTAL LINE - left to right                                                                  //                  x___x
		                                                                                                              //                  |
		                                                                                                              //                  |
                                                                                                                      //                  |
		int x_initial = X_0 + midpoint;                                                                               //                  x
		int y_initial = Y_0 + height;                                                                                 //                 /|
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |
                                                                                                                      //          x----x--x--------------x 
		int x_final = X_0 + midpoint + FLAG_HEIGHT;                                                                   //           \                    /
		int y_final = Y_0 + height;                                                                                   //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
		
	}
	
	private void drawRightVertical() {
		// TODO DRAW VERTICAL LINE - top to bottom                                                                    //                  x___x
                                                                                                                      //                  |   |
                                                                                                                      //                  |   
                                                                                                                      //                  |
		int x_initial = X_0 + midpoint + FLAG_HEIGHT;                                                                 //                  x
		int y_initial = Y_0 + height;                                                                                 //                 /|
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |
                                                                                                                      //          x----x--x--------------x 
		int x_final = X_0 + midpoint + FLAG_HEIGHT;                                                                   //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE + big_sail_height;                                                         //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
		
	}
	
	private void drawLowerHorizontal() {
		// TODO DRAW HORIZONTAL LINE - right to left                                                                  //                  x___x
																				                                      //                  |___|
                                                                                                                      //                  |   
                                                                                                                      //                  |
		int x_initial = X_0 + midpoint + FLAG_HEIGHT;                                                                 //                  x
		int y_initial = Y_0 + EDGE_CLEARANCE + big_sail_height;                                                       //                 /|
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |
                                                                                                                      //          x----x--x--------------x 
		int x_final = X_0 + midpoint;                                                                                 //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE + big_sail_height;                                                         //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}

	private void plotBigSail() {
		
		// TODO DRAW BIG SAIL HYPOTNEUSE - right top to left bottom                                                   //                  x___x
                                                                                                                      //                  |___|
                                                                                                                      //                  |\   
                                                                                                                      //                  | \
		int x_initial = X_0 + midpoint;                                                                               //                  x  \
		int y_initial = Y_0 + EDGE_CLEARANCE + big_sail_height;                                                       //                 /|   \
		Coord start = new Coord(pc, x_initial, y_initial);                                                            //                / |    \
                                                                                                                      //          x----x--x-----x--------x 
		int x_final = X_0 + (width - EDGE_CLEARANCE);                                                                 //           \                    /
		int y_final = Y_0 + EDGE_CLEARANCE;                                                                           //            x------------------x
		Coord end = new Coord(pc, x_final, y_final);
	}
	
}