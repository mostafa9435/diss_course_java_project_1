package de.tuhh.diss.plotbot.shape;

import lejos.nxt.Motor;
import lejos.nxt.TouchSensor;
import lejos.util.Delay;
import lejos.util.Matrix;
import lejos.util.Timer;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import de.tuhh.diss.plotbot.shape.Plottable;
import de.tuhh.diss.plotbot.PlotbotControl;
import de.tuhh.diss.plotbot.shape.Calibration;

public class Line {

	private PlotbotControl pc;
	private Coord start;
	private Coord end;

	public Line(PlotbotControl pc, Coord start, Coord end) {
		this.pc = pc;
		this.start = start;
		this.end = end;
	}

	public void drawLine() {
		if (this.start.getX() == this.end.getX()) {
			drawVerticalLine();
		}

		else if (this.start.getY() == this.end.getY()) {
			drawHorizontalLine();
		}

		else {
			drawInclinedLine();
		}
	}

	public void drawVerticalLine() {
		// STEP 1: Descend the pen.
		/*
		 * Motor.B.resetTachoCount(); Motor.B.setSpeed(400); LCD.clearDisplay();
		 * Motor.B.backward(); // descend pen while (Motor.B.getTachoCount() <
		 * pc.MAX_PEN_HEIGHT) { LCD.drawInt(Motor.B.getTachoCount(), 0, 0); }
		 * Motor.B.stop();
		 */

		int lengthToDraw = this.end.getY() - this.start.getY();
		double angle = (lengthToDraw * pc.WHEEL_GEAR_RATIO) / (pc.WHEEL_RADIUS);
		angle *= pc.RADIANS_TO_DEGREES;
		Motor.C.rotate((int) Math.round(angle));
		Delay.msDelay(5000);
		pc.angleToXy(Motor.A.getTachoCount(), Motor.C.getTachoCount());

		/*
		 * // STEP 2: Move forward/backward. int y_start = start.getY(); int
		 * y_end = end.getY(); int distance = Math.abs(y_start - y_end); int
		 * theta_motor = pc.getMappedWheelMotorAngle(distance);
		 * Motor.C.setSpeed(400);
		 * 
		 * if (y_end < y_start) { theta_motor = -1 * theta_motor;
		 * Motor.C.rotate(theta_motor); } if (y_end > y_start) {
		 * Motor.C.rotate(theta_motor); }
		 * 
		 * // STEP 3: Ascend the pen Motor.B.setSpeed(400); LCD.clearDisplay();
		 * Motor.B.backward(); // descend pen while (true) { if
		 * (pc.getPenSensor().isPressed()) { Motor.B.stop(); } }
		 */
		/////////////////////////////////
		////// LINE HAS BEEN DRAWN //////
		/////////////////////////////////
	}

	public void drawHorizontalLine() {

		// first step: Draw the mini arc (visibly horizontal line)
		// ********************************************************

		Motor.A.setSpeed(400);
		Motor.C.setSpeed(400);

		int initialX = this.start.getX(); // X coordinates of the start point of
											// the line
		int initialY = this.start.getY(); // Y coordinates of the start point of
											// the line

		int finalX = this.end.getX(); // X coordinates of the end point of the
										// line
		int finalY = this.end.getY(); // X coordinates of the end point of the
										// line

		int lengthToDraw = (finalX > initialX) ? finalX - initialX : initialX - finalX;
		int rotateIncrement = (finalX > initialX) ? -1 : 1;

		while (!(pc.getArmSensor().isPressed()) && (lengthToDraw > 0)) {

			Motor.A.rotate(50 * rotateIncrement);
			Delay.msDelay(1000);
			pc.angleToXy(Motor.A.getTachoCount(), Motor.C.getTachoCount()); // to
																			// update
																			// absolute
																			// pen
																			// coordinates
			lengthToDraw = (finalX > initialX) ? finalX - (int) pc.getxCurrent() : (int) pc.getxCurrent() - finalX; // update
																													// length
																													// left
																													// to
																													// be
																													// drawn
			LCD.clearDisplay();
			LCD.drawInt((int) pc.getyCurrent(), 0, 0);

			Delay.msDelay(2000);

			// vertical Line drawn
			double angle = ((initialY - pc.getyCurrent()) * pc.WHEEL_GEAR_RATIO) / (pc.WHEEL_RADIUS);
			angle *= pc.RADIANS_TO_DEGREES;
			Motor.C.rotate((int) Math.round(angle));
			Delay.msDelay(2000);
			pc.angleToXy(Motor.A.getTachoCount(), Motor.C.getTachoCount());

		}

		LCD.clearDisplay();
		LCD.drawString("7ashash2", 0, 0);
		Delay.msDelay(2000);

		pc.angleToXy(Motor.A.getTachoCount(), Motor.C.getTachoCount());

		//////////////////////////////// **************************************///////////////////////

		// second step: compensate the vertical deflection
		// ************************************************

		/*
		 * Motor.C.setSpeed(200); Motor.C.resetTachoCount();
		 * 
		 * 
		 * 
		 * Motor.C.forward(); while(initialY >= currentY){ currentY += (
		 * Motor.C.getTachoCount() * pc.RATIO_TRANSLATION_MOVEMENT ); }
		 * Motor.C.stop();
		 */

	}

	public void drawInclinedLine() {
		double angles[] = pc.xyToAngle(end.getX() - start.getX(), end.getY() - start.getY());
		double maxAngleToRotateA = angles[0];
		double maxAngleToRotateC = angles[1];
		Motor.A.setSpeed(400);
		Motor.C.setSpeed(400);

		while (Math.abs(Motor.A.getTachoCount()) < Math.abs(maxAngleToRotateA)
				|| Math.abs(Motor.C.getTachoCount()) < Math.abs(maxAngleToRotateC)) {
			if (Math.abs(Motor.A.getTachoCount()) < Math.abs(maxAngleToRotateA)) {
				Motor.A.rotate((int) Math.round(maxAngleToRotateA / 100));
			}
			if (Math.abs(Motor.C.getTachoCount()) < Math.abs(maxAngleToRotateC)) {
				Motor.C.rotate((int) Math.round(maxAngleToRotateC / 100));
			}

		}
	}
}
